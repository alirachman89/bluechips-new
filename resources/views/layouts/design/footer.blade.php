<!-- Start Footer Section -->
<div class="section footer-scetion footer-particle section-pad-sm section-bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xl-4 res-l-bttm">
                <a class="footer-brand animated" data-animate="fadeInUp" data-delay="0" href="./">
                    <img class="logo logo-light logo-size" alt="logo" src="{{asset('images/main/logo-white-181x40.jpg')}}" srcset="{{asset('images/main/logo-white-181x40.jpg')}} 2x">
                </a>
                <ul class="social">
                    <li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="https://facebook.com/bchiptoken/"><em class="fab fa-facebook-f"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="https://twitter.com/bchiptoken"><em class="fab fa-twitter"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="https://www.reddit.com/user/bluechip-token"><em class="fab fa-reddit"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/BCHIPTOKEN"><em class="fab fa-github"></em></a></li>
                    <!--<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="https://bitcointalk.org/index.php?topic=5048467.msg46764067#msg46764067"><em class="fab fa-bitcoin"></em></a></li>-->
                    <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@bluechipstoken/"><em class="fab fa-medium-m"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="http://t.me/BluechipsToken/"><em class="fab fa-telegram"></em></a></li>
                </ul>
            </div><!-- .col  -->
            <div class="col-sm-6 col-xl-4 res-l-bttm">
                <div class="footer-subscription">
                    <h6 class="animated" data-animate="fadeInUp" data-delay=".6">Subscribe to our newsleter</h6>
                    <form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form animated" data-animate="fadeInUp" data-delay=".7">
                        <input type="text" name="youremail" class="input-round required email" placeholder="Enter your email" >
                        <input type="text" class="d-none" name="form-anti-honeypot" value="">
                        <button type="submit" class="btn btn-plane">Subscribe</button>
                        <div class="subscribe-results"></div>
                    </form>
                </div>
            </div><!-- .col  -->
            <div class="col-xl-4">
                <ul class="link-widget animated" data-animate="fadeInUp" data-delay=".8">
                    <li><a href="#intro" class="menu-link">BLUECHIPS</a></li>
                    <li><a href="#apps" class="menu-link">BLUECHIPS Apps</a></li>
                    <li><a href="#tokensale" class="menu-link">Tokens Sale</a></li>
                    <li><a href="{{asset('doc/whitepaper.pdf')}}" class="menu-link" download="">Whitepaper</a></li>
                    <li><a href="#contact" class="menu-link">Contact</a></li>
                    <li><a href="#roadmap" class="menu-link">Roadmap</a></li>
                    <li><a href="#team" class="menu-link">Teams</a></li>
                </ul>
            </div><!-- .col  -->
        </div><!-- .row  -->
        <div class="gaps size-2x"></div>
        <div class="row">
            <div class="col-md-7">
					<span class="copyright-text animated" data-animate="fadeInUp" data-delay=".9">
						Copyright &copy; 2018, BlueChips
					</span>
            </div><!-- .col  -->
            <div class="col-md-5 text-right mobile-left">
                <ul class="footer-links animated" data-animate="fadeInUp" data-delay="1">
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                </ul>
            </div><!-- .col  -->
        </div><!-- .row  -->
    </div><!-- .container  -->
</div>
<!-- End Section -->

<!-- Preloader !remove please if you do not want -->
<div id="preloader">
    <div id="loader"></div>
    <div class="loader-section loader-top"></div>
    <div class="loader-section loader-bottom"></div>
</div>
<!-- Preloader End -->

<a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

<!-- JavaScript (include all script here) -->
<!-- JavaScript (include all script here) -->
<script src="{{asset('assets/js/jquery.bundle.js?ver=142')}}"></script>
<script src="{{asset('assets/js/script.js?ver=142')}}"></script>
<script src="{{asset('assets/js/return-to-top.js')}}"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>

<!--script for menu -->

<script type="application/javascript">
    $(document).ready(function () {
        var scroll_start = 0;
        var startchange = $('.change-it-now');
        var offset = startchange.offset();

        if (startchange.length) {
            $(document).scroll(function () {
                scroll_start = $(this).scrollTop();
                if(scroll_start > offset.top){
                    $('.navbar-live').css('color', '#fff');
                    $('.tele').css('color', '#fff');
//                    $('.navbar-custom').css('display','block')
                    $('.navbar-custom').hide();
                } else {
                    $('.navbar-live').css('color', 'transparent');
//                    $('.navbar-custom').css('display','none')
                    $('.navbar-custom').show();

                }
            })
        }
    })



</script>
<!--script for menu -->




<!-- end off script for menu -->