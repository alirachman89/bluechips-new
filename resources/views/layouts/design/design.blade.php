<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="BlueChips Token | The Masterpiece of Decentralized in Entertainment">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}">
    <!-- Site Title  -->
    <title>BlueChips - Token</title>
    <!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor.bundle.css?ver=142')}}">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css?ver=142')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.php')}}">
    <link rel="stylesheet" href="{{asset('assets/css/theme.css?ver=142')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>

    <!-- js for progress bar -->
    <script>
        $(function() {
            $(".meter > span").each(function() {
                $(this)
                    .data("origWidth", $(this).width())
                    .width(0)
                    .animate({
                        width: $(this).data("origWidth")
                    }, 1200);
            });
        });
    </script>
    <!-- end of js for progress bar -->



    <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    <style>

        .navbar-live {
            color: transparent;
            display: inline-block;
        }

        .tele {
            color: transparent;
        }
    </style>


</head>
<body class="theme-dark io-azure io-azure-pro" data-spy="scroll" data-target="#mainnav" data-offset="80">

@include('layouts.design.header')
@yield('content')
@include('layouts.design.footer')
</body>
</html>



