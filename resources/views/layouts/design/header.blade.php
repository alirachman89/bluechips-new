<!-- Header -->
<header class="site-header is-sticky">

    <!-- Place Particle Js -->
    <div id="particles-js" class="particles-container particles-js"></div>

    <!-- Navbar -->
    <div class="navbar navbar-expand-lg is-transparent navbar navbar-fixed-top" id="mainnav">
        <nav class="container">

            <a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./">
                <img class="logo logo-dark" alt="logo" src="{{asset('images/main/logo_bluechips_4.png')}}" srcset="{{asset('images/main/logo_bluechips_4.png')}} 2x">
                <img class="logo logo-light" alt="logo" src="{{asset('images/main/logo_bluechips_4.png')}}" srcset="{{asset('images/main/logo_bluechips_4.png')}} 2x">
            </a>


            <div class="collapse navbar-collapse justify-content-end change-nav" id="navbarToggle">
                <ul class="navbar-nav animated remove-animation navbar-custom" data-animate="fadeInDown" data-delay=".9">
                    <li class="nav-item"><a class="nav-link menu-link" href="#intro">What is BLUECHIPS<span class="sr-only">(current)</span></a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#tokenSale">Token Sale</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#roadmap">Roadmap</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#apps">Apps</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#team">Team</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#contact">Contact</a></li>
                </ul>
                <ul class="navbar-nav animated remove-animation navbar-live">
                    <li class="nav-item">TOKEN SALE IS LIVE NOW Join Our Telegram <a href="http://t.me/BluechipsToken/"><em class="fab fa-telegram tele" style="font-size: 20px;"></em></a></li>
                </ul>
            </div>

        </nav>
    </div>
    <!-- End Navbar -->


    <!-- Banner/Slider -->
    <div id="header" class="banner banner-full d-flex align-items-center change-it-now">
        <div class="container">
            <div class="banner-content">
                <div class="row align-items-center mobile-center">
                    <div class="col-lg-7 col-md-7 order-lg-first">
                        <div class="header-txt">
                            <h1 class="animated banner-text" data-animate="fadeInUp" data-delay="1.55">The Masterpiece <br>Of Decentralized in Entertainment</h1>

                            <ul class="btns animated" data-animate="fadeInUp" data-delay="1.75">
                                <li><a href="{{asset('doc/whitepaper.pdf')}}" class="btn" tabindex="-1">WHITEPAPER</a></li>
                                <li><a href="https://medium.com/@bluechipstoken/bluechips-token-airdrop-69c721e8888e?source=user_profile---------10------------------" class="btn">AIRDROP</a></li>
                            </ul>
                        </div>
                    </div><!-- .col  -->
                    <div class="col-lg-5 order-first res-m-bttm-lg">
                        <div class="countdown-box text-center animated" data-animate="fadeInUp" data-delay=".3">
                            <h6>TOKEN SALE PHASE 1</h6>
                            <div class="token-countdown d-flex align-content-stretch" data-date="2018/11/10 13:00:00 UTC"></div>
                            <div class="progress">
                                <!--<div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0.03%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>-->
                                <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 3%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="soft-left" style="float: left;">SOFT CAP: 350 ETH</div>
                            <div class="hard-right" style="float: right;">HARD CAP: 600 ETH</div>
                        <a href="{{url('https://etherscan.io/address/0x5ef227f7ce4e96c9ce90e32d4850545a6c5d099b')}}" class="btn btn-alt btn-sm">BUY TOKEN NOW</a>
                        </div>
                        <ul class="social social-banner">
                            <li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="https://facebook.com/bchiptoken/"><em class="fab fa-facebook-f"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="https://twitter.com/bchiptoken"><em class="fab fa-twitter"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="https://www.reddit.com/user/bluechip-token"><em class="fab fa-reddit"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/BCHIPTOKEN"><em class="fab fa-github"></em></a></li>
                            <!--<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="https://bitcointalk.org/index.php?topic=5048467.msg46764067#msg46764067"><em class="fab fa-bitcoin"></em></a></li>-->
                            <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@bluechipstoken/"><em class="fab fa-medium-m"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="http://t.me/BluechipsToken/"><em class="fab fa-telegram"></em></a></li>
                        </ul>
                    </div><!-- .col  -->
                </div><!-- .row  -->
            </div><!-- .banner-content  -->
        </div><!-- .container  -->
    </div>
    <!-- End Banner/Slider -->


    <a href="#intro" class="scroll-down menu-link">SCROLL DOWN</a>
</header>
<!-- End Header -->