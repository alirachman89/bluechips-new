<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="BlueChips Token | The Masterpiece of Decentralized in Entertainment">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}">
    <!-- Site Title  -->
    <title>BlueChips - Token</title>
    <!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor.bundle.css?ver=142')}}">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css?ver=142')}}">
    <link rel="stylesheet" href="{{asset('assets/css/theme.css?ver=142')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">


</head>
<body class="theme-dark io-azure io-azure-pro" data-spy="scroll" data-target="#mainnav" data-offset="80">

<!-- Header -->
<header class="site-header is-sticky">

    <!-- Place Particle Js -->
    <div id="particles-js" class="particles-container particles-js"></div>

    <!-- Navbar -->
    <div class="navbar navbar-expand-lg is-transparent" id="mainnav">
        <nav class="container">

            <a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./">
                <img class="logo logo-dark" alt="logo" src="{{asset('images/main/logo_bluechips_4.png')}}" srcset="{{asset('images/main/logo_bluechips_4.png')}} 2x">
                <img class="logo logo-light" alt="logo" src="{{asset('images/main/logo_bluechips_4.png')}}" srcset="{{asset('images/main/logo_bluechips_4.png')}} 2x">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">
					<span class="navbar-toggler-icon">
						<span class="ti ti-align-justify"></span>
					</span>
            </button>

            <!--<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">
                <ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".9">
                    <li class="nav-item"><a class="nav-link menu-link" href="#intro">What is BLUECHIPS<span class="sr-only">(current)</span></a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#tokenSale">Token Sale</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#roadmap">Roadmap</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#apps">Apps</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#team">Team</a></li>
                    <li class="nav-item"><a class="nav-link menu-link" href="#contact">Contact</a></li>
                </ul>-->
                <!--<ul class="navbar-nav navbar-btns animated remove-animation" data-animate="fadeInDown" data-delay="1.15">
                    <li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" href="#">Sign up</a></li>
                    <li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" href="#">Log in</a></li>
                </ul>-->
            <!--</div>-->

        </nav>
    </div>
    <!-- End Navbar -->


    <!-- Banner/Slider -->
    <!--<div id="header" class="banner  d-flex align-items-center">
        <div class="container">
            <div class="banner-content">
                <div class="row align-items-center mobile-center">
                    <div class="col-lg-7 col-md-7 order-lg-first">
                        <div class="header-txt">

                            <img class="image-btluechips" src="{{asset('images/main/bluechips-gold.png')}}" alt="BlueChips">

                            <ul class="btns animated" data-animate="fadeInUp" data-delay="1.75">
                                <li><a href="{{asset('doc/whitepaper.pdf')}}" class="btn" tabindex="-1">WHITEPAPER</a></li>
                                <li><a href="https://medium.com/@bchip.project/bluechips-token-airdrop-c41e2c12a2cc" class="btn">AIRDROP</a></li>
                            </ul>
                        </div>
                    </div><!-- .col  -->
                   <!-- <div class="col-lg-5">
                        <div class="countdown-box text-center animated" data-animate="fadeInUp" data-delay=".3">
                            <h6>BLUECHIPS will end on</h6>
                            <div class="token-countdown d-flex align-content-stretch" data-date="2018/11/05 12:00:00 UTC"></div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0.2%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="soft-left" style="float: left;">SOFT CAP: 350 ETH</div>
                            <div class="hard-right" style="float: right;">HARD CAP: 600 ETH</div>
                        a href="{{url('buytokens')}}" class="btn btn-alt btn-sm">BUY TOKEN NOW</a>
                        </div>-->
                       <!-- <ul class="social social-banner">
                            <li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="https://facebook.com/bchiptoken/"><em class="fab fa-facebook-f"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="https://twitter.com/bchiptoken"><em class="fab fa-twitter"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="https://www.reddit.com/user/bluechip-token"><em class="fab fa-reddit"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/BCHIPTOKEN"><em class="fab fa-github"></em></a></li>
                            <!--<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="https://bitcointalk.org/index.php?topic=5048467.msg46764067#msg46764067"><em class="fab fa-bitcoin"></em></a></li>-->
                            <!--<li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@bluechipstoken/"><em class="fab fa-medium-m"></em></a></li>
                            <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="http://t.me/BluechipsToken/"><em class="fab fa-telegram"></em></a></li>
                        </ul>-->
                   <!-- </div>--><!-- .col  -->
               <!-- </div>--><!-- .row  -->
            <!--</div>--><!-- .banner-content  -->
        <!--</div>--><!-- .container  -->
    <!--</div>-->
    <!-- End Banner/Slider -->


    <!--<a href="#intro" class="scroll-down menu-link">SCROLL DOWN</a>-->
</header>
<!-- End Header -->




<div class="section section-pad section-bg-alt section-pro-alt roadmap-section">
    <div class="container purchase-text">
        <h1>Purchase Token</h1>
        <ol>
            <li>1. Ensure you have at least 0.01 ETH plus GAS fee in your ERC-20 Supported Wallet (Not From Exchange)</li>
            <li>2. Set GAS = 100,000, GWEI = 10 or check https://ethgasstation.info/ for every new transaction</li>
            <li>3. Send ETH only to this contract address :
                <br>0x5ef227f7ce4e96c9ce90e32d4850545a6c5d099b
                <br>SOFTCAP : 350 ETH
                <br>HARDCAP : 600 ETH
                <br>DISTRIBUTION :
                <br>Phase 1 : 02/10 - 10/11
                <br>1 ETH = 10,000,000 BCHIP + Bonus 20%
                <br>Limit Supply : 2,000,000,000 BCHIP
                <br>Phase 2 : 11/11 - 25/11
                <br>1 ETH = 10,000,000 BCHIP
                <br>Limit Supply : 4,000,000,000 BCHIP
                <br><i class="fa fa-fire" style="color: red;"></i> ALL UNSOLD TOKENS WILL BE BURNED <i class="fa fa-fire" style="color: red;"></i>
                <!--<br> 1 ETH = 12,000,000 BCHIP
                <br> 0.1 ETH = 1,200,000 BCHIP
                <br> 0.01 ETH = 120,000 BCHIP -->
            </li>

        </ol>
    </div>
    <div class="mask-ov-right mask-ov-s2"></div><!-- .mask overlay -->
</div>





<!-- Start Section -->
<div class="section footer-scetion footer-particle section-pad-sm section-bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xl-4 res-l-bttm">
                <a class="footer-brand animated" data-animate="fadeInUp" data-delay="0" href="./">
                    <img class="logo logo-light logo-size" alt="logo" src="{{asset('images/main/logo-white-181x40.jpg')}}" srcset="{{asset('images/main/logo-white-181x40.jpg')}} 2x">
                </a>
                <ul class="social">
                    <li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="https://facebook.com/bchiptoken/"><em class="fab fa-facebook-f"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="https://twitter.com/bchiptoken"><em class="fab fa-twitter"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="https://www.reddit.com/user/bluechip-token"><em class="fab fa-reddit"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/BCHIPTOKEN"><em class="fab fa-github"></em></a></li>
                    <!--<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="https://bitcointalk.org/index.php?topic=5048467.msg46764067#msg46764067"><em class="fab fa-bitcoin"></em></a></li>-->
                    <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@bluechipstoken/"><em class="fab fa-medium-m"></em></a></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="http://t.me/BluechipsToken/"><em class="fab fa-telegram"></em></a></li>
                </ul>
            </div><!-- .col  -->
            <div class="col-sm-6 col-xl-4 res-l-bttm">
                <div class="footer-subscription">
                    <h6 class="animated" data-animate="fadeInUp" data-delay=".6">Subscribe to our newsleter</h6>
                    <form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form animated" data-animate="fadeInUp" data-delay=".7">
                        <input type="text" name="youremail" class="input-round required email" placeholder="Enter your email" >
                        <input type="text" class="d-none" name="form-anti-honeypot" value="">
                        <button type="submit" class="btn btn-plane">Subscribe</button>
                        <div class="subscribe-results"></div>
                    </form>
                </div>
            </div><!-- .col  -->
            <div class="col-xl-4">
                <ul class="link-widget animated" data-animate="fadeInUp" data-delay=".8">
                    <li><a href="#intro" class="menu-link">BLUECHIPS</a></li>
                    <li><a href="#apps" class="menu-link">BLUECHIPS Apps</a></li>
                    <li><a href="#tokensale" class="menu-link">Tokens Sale</a></li>
                    <li><a href="{{asset('doc/whitepaper.pdf')}}" class="menu-link" download="">Whitepaper</a></li>
                    <li><a href="#contact" class="menu-link">Contact</a></li>
                    <li><a href="#roadmap" class="menu-link">Roadmap</a></li>
                    <li><a href="#team" class="menu-link">Teams</a></li>
                </ul>
            </div><!-- .col  -->
        </div><!-- .row  -->
        <div class="gaps size-2x"></div>
        <div class="row">
            <div class="col-md-7">
					<span class="copyright-text animated" data-animate="fadeInUp" data-delay=".9">
						Copyright &copy; 2018, BlueChips
					</span>
            </div><!-- .col  -->
            <div class="col-md-5 text-right mobile-left">
                <ul class="footer-links animated" data-animate="fadeInUp" data-delay="1">
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                </ul>
            </div><!-- .col  -->
        </div><!-- .row  -->
    </div><!-- .container  -->
</div>
<!-- End Section -->

<!-- Preloader !remove please if you do not want -->
<div id="preloader">
    <div id="loader"></div>
    <div class="loader-section loader-top"></div>
    <div class="loader-section loader-bottom"></div>
</div>
<!-- Preloader End -->

<a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

<!-- JavaScript (include all script here) -->
<!-- JavaScript (include all script here) -->
<script src="{{asset('assets/js/jquery.bundle.js?ver=142')}}"></script>
<script src="{{asset('assets/js/script.js?ver=142')}}"></script>
<script src="{{asset('assets/js/return-to-top.js')}}"></script>

</body>
</html>
