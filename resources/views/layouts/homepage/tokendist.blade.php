<!-- Start TOken distribution Section -->
<div class="section section-pad section-bg token-section section-gradiant" id="tokenSale">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 col-md-6 col-sm-12 center-class">
                <div class="single-chart light">
                    <h3 class="sub-heading" style="margin-bottom: -2px;">TOKEN DETAILS</h3>
                    <img class="image-bluechips" src="{{asset('images/bluechips_token.png')}}" alt="Token Details">

                    <ul class="ul-style">
                        <li style="list-style-type: disc; text-align: left;"><span>Name :</span>BLUECHIPS</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Tickers Symbol :</span>BCHIP</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Token Background :</span>ETHEREUM (ERC20)</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Smart Contract :</span>0x5ef227f7ce4e96c9ce90e32d4850545a6c5d099b</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Decimal :</span>8</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Total Supply :</span>10.000.000.000</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Number of Token Sale :</span>6.000.000.000 (60%)</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Softcap :</span>350 ETH</li>
                        <li style="list-style-type: disc; text-align: left;"><span>Hardcap :</span>600 ETH</li>
                    </ul>
                </div>
                <div class="row">
                    <a href="{{url('buytoken')}}" class="btn btn-alt btn-sm">BUY TOKEN NOW</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 center-class">
                <h3 class="sub-heading">TOKEN DISTRIBUTION</h3>
               <img class="" src="{{asset('images/main/1.png')}}" alt="BTube">
            </div><!-- .col  -->
        </div><!-- .row  -->

    </div><!-- .container  -->
</div>
<!-- Start Section -->