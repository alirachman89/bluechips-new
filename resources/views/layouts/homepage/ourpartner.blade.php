<!-- Start Our Partner Session -->
<div class="section section-pad section-bg-dark" id="partners">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 offset-md-3">
                <div class="section-head">
                    <h6 class="section-title-sm animated" data-animate="fadeInUp" data-delay=".0">Supported Wallet</h6>
                </div>
            </div>
        </div><!-- .row  -->
        <div class="partner-list">
            <div class="row text-center">
                <div class="col-sm col-6">
                    <div class="single-partner animated" data-animate="fadeInUp" data-delay=".1">
                        <img src="{{asset('images/ether_wallet_2.png')}}" alt="partner">
                    </div>
                </div><!-- .col  -->
                <div class="col-sm col-6">
                    <div class="single-partner animated" data-animate="fadeInUp" data-delay=".2">
                        <img src="{{asset('images/imtoken_2.png')}}" alt="partner">
                    </div>
                </div><!-- .col  -->
                <div class="col-sm col-6">
                    <div class="single-partner animated" data-animate="fadeInUp" data-delay=".3">
                        <img src="{{asset('images/metamask.png')}}" alt="partner">
                    </div>
                </div><!-- .col  -->
                <div class="col-sm col-6">
                    <div class="single-partner animated" data-animate="fadeInUp" data-delay=".4">
                        <img src="{{asset('images/mycrypto_2.png')}}" alt="partner">
                    </div>
                </div><!-- .col  -->
                <div class="col-sm col-6">
                    <div class="single-partner animated" data-animate="fadeInUp" data-delay=".5">
                        <img src="{{asset('images/trust_wallet_2.png')}}" alt="partner">
                    </div>
                </div><!-- .col  -->
            </div><!-- .row  -->
        </div><!-- .partner-list  -->
    </div><!-- .container  -->
</div>
<!-- End Session -->