<!DOCTYPE html>
<!--
    Name: GoodGames - Game Portal / Store HTML Template
    Version: 1.4.0
    Author: nK
    Website: https://nkdev.info/
    Purchase: https://themeforest.net/item/goodgames-portal-store-html-gaming-template/17704593?ref=_nK
    Support: https://nk.ticksy.com/
    License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
    Copyright 2018.
-->

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Bfun | BGames</title>

    <meta name="description" content="BFun">
    <meta name="keywords" content="game, gaming">
    <meta name="author" content="_nK">

    <link rel="icon" type="image/png" href="{{asset('images/main/favicon.png')}}">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7cOpen+Sans:400,700" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/bfun/vendor/bootstrap/dist/css/bootstrap.min.css')}}">

    <!-- FontAwesome -->
    <script defer src="{{asset('assets/bfun/vendor/fontawesome-free/js/all.js')}}"></script>
    <script defer src="{{asset('assets/bfun/vendor/fontawesome-free/js/v4-shims.js')}}"></script>

    <!-- IonIcons -->
    <link rel="stylesheet" href="{{asset('assets/bfun/vendor/ionicons/css/ionicons.min.css')}}">

    <!-- Flickity -->
    <link rel="stylesheet" href="{{asset('assets/bfun/vendor/flickity/dist/flickity.min.css')}}">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bfun/vendor/photoswipe/dist/photoswipe.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bfun/vendor/photoswipe/dist/default-skin/default-skin.css')}}">

    <!-- Seiyria Bootstrap Slider -->
    <link rel="stylesheet" href="{{asset('assets/bfun/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css')}}">

    <!-- Summernote -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bfun/vendor/summernote/dist/summernote-bs4.css')}}">

    <!-- GoodGames -->
    <link rel="stylesheet" href="{{asset('assets/bfun/css/goodgames.css')}}">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{asset('assets/bfun/css/custom.css')}}">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="{{asset('assets/bfun/vendor/jquery/dist/jquery.min.js')}}"></script>


</head>


<!--
    Additional Classes:
        .nk-page-boxed
-->
<body>





<!--
    Additional Classes:
        .nk-header-opaque
-->
<header class="nk-header nk-header-opaque">



    <!-- START: Top Contacts -->

    <!-- END: Top Contacts -->



    <!--
        START: Navbar

        Additional Classes:
            .nk-navbar-sticky
            .nk-navbar-transparent
            .nk-navbar-autohide
    -->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide">

    </nav>
    <!-- END: Navbar -->

</header>



<!--
START: Navbar Mobile

Additional Classes:
.nk-navbar-left-side
.nk-navbar-right-side
.nk-navbar-lg
.nk-navbar-overlay-content
-->
<div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-right-side nk-navbar-overlay-content d-lg-none">
    <div class="nano">
        <div class="nano-content">
            <a href="bfun" class="nk-nav-logo">
                <img src="assets/images/logo.png" alt="" width="120">
            </a>
            <div class="nk-navbar-mobile-content">
                <ul class="nk-nav">
                    <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END: Navbar Mobile -->



<div class="nk-main">

    <div class="nk-gap-2"></div>



    <div class="container">

        <!-- START: Image Slider -->
        <div class="nk-image-slider" data-autoplay="8000">


            <div class="">
                <img src="{{asset('assets/images/banner/banner1.jpg')}}" alt="" class="nk-image-slider-img" data-thumb="assets/images/banner/banner1.jpg">
            </div>

        </div>
        <!-- END: Image Slider -->

        <!-- START: Categories -->
        <div class="nk-gap-2"></div>
        <div class="row vertical-gap">
            <div class="col-lg-4">
                <div class="nk-feature-1">
                    <div class="nk-feature-icon">
                        <img src="{{asset('images/main/mouse_logo.png')}}" alt="">
                    </div>
                    <div class="nk-feature-cont">
                        <h3 class="nk-feature-title"><a href="#">PC</a></h3>
                        <h4 class="nk-feature-title text-main-1"><a href="#">View Games</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="nk-feature-1">
                    <div class="nk-feature-icon">
                        <img src="{{asset('images/main/android_logo.png')}}" alt="">
                    </div>
                    <div class="nk-feature-cont">
                        <h3 class="nk-feature-title"><a href="#">ANDROID</a></h3>
                        <h4 class="nk-feature-title text-main-1"><a href="#">View Games</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="nk-feature-1">
                    <div class="nk-feature-icon">
                        <img src="{{asset('images/main/logo_apple.png')}}" alt="">
                    </div>
                    <div class="nk-feature-cont">
                        <h3 class="nk-feature-title"><a href="#">IOS</a></h3>
                        <h4 class="nk-feature-title text-main-1"><a href="#">View Games</a></h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Categories -->

        <!-- START: Latest News -->
        <div class="nk-gap-2"></div>
        <h3 class="nk-decorated-h-2"><span><span class="text-main-1">Latest</span> Games</span></h3>
        <div class="nk-gap"></div>



        <div class="nk-gap-2"></div>
        <div class="nk-blog-grid">
            <div class="row">


                <div class="col-md-6 col-lg-3">
                    <!-- START: Post -->
                    <div class="nk-blog-post">
                        <a href="#" class="nk-post-img">
                            <img src="{{asset('images/main/indie_games.png')}}" alt="He made his passenger captain of one">
                            <span class="nk-post-comments-count">13</span>

                            <span class="nk-post-categories">
                                    <span class="bg-main-5">Indie</span>
                                </span>

                        </a>
                        <div class="nk-gap"></div>
                        <h2 class="nk-post-title h4"><a href="#">He made his passenger captain of one</a></h2>
                        <div class="nk-post-text">
                            <p>Just then her head struck against the roof of the hall: in fact she was now more than nine feet high, and she at once took up the little golden key and hurried off to the garden door...</p>
                        </div>
                        <div class="nk-gap"></div>
                        <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">Read More</a>

                    </div>
                    <!-- END: Post -->
                </div>


                <div class="col-md-6 col-lg-3">
                    <!-- START: Post -->
                    <div class="nk-blog-post">
                        <a href="#" class="nk-post-img">
                            <img src="{{asset('images/main/racing.png')}}" alt="At first, for some time, I was not able to answer">
                            <span class="nk-post-comments-count">0</span>

                            <span class="nk-post-categories">
                                    <span class="bg-main-5">Racing</span>
                                </span>

                        </a>
                        <div class="nk-gap"></div>
                        <h2 class="nk-post-title h4"><a href="#">At first, for some time, I was not able to answer</a></h2>
                        <div class="nk-post-text">
                            <p>This little wandering journey, without settled place of abode, had been so unpleasant to me, that my own house, as I called it to myself, was a perfect settlement to me compared to that...</p>
                        </div>
                        <div class="nk-gap"></div>
                        <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">Read More</a>

                    </div>
                    <!-- END: Post -->
                </div>


                <div class="col-md-6 col-lg-3">
                    <!-- START: Post -->
                    <div class="nk-blog-post">
                        <a href="#" class="nk-post-img">
                            <img src="{{asset('images/main/moba.png')}}" alt="At length one of them called out in a clear">
                            <span class="nk-post-comments-count">0</span>

                            <span class="nk-post-categories">
                                    <span class="bg-main-6">MOBA</span>
                                </span>

                        </a>
                        <div class="nk-gap"></div>
                        <h2 class="nk-post-title h4"><a href="#">At length one of them called out in a clear</a></h2>
                        <div class="nk-post-text">
                            <p>Just then her head struck against the roof of the hall: in fact she was now more than nine feet high, and she at once took up the little golden key and hurried off to the garden door...</p>
                        </div>
                        <div class="nk-gap"></div>
                        <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">Read More</a>

                    </div>
                    <!-- END: Post -->
                </div>


                <div class="col-md-6 col-lg-3">
                    <!-- START: Post -->
                    <div class="nk-blog-post">
                        <a href="#" class="nk-post-img">
                            <img src="{{asset('images/main/adventure.png')}}" alt="For good, too though, in consequence">
                            <span class="nk-post-comments-count">0</span>

                            <span class="nk-post-categories">
                                    <span class="bg-main-2">Adventure</span>
                                </span>

                        </a>
                        <div class="nk-gap"></div>
                        <h2 class="nk-post-title h4"><a href="#">For good, too though, in consequence</a></h2>
                        <div class="nk-post-text">
                            <p>This little wandering journey, without settled place of abode, had been so unpleasant to me, that my own house, as I called it to myself, was a perfect settlement to me compared to that...</p>
                        </div>
                        <div class="nk-gap"></div>
                        <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">Read More</a>

                    </div>
                    <!-- END: Post -->
                </div>

            </div>
        </div>
        <!-- END: Latest News -->

        <div class="nk-gap-2"></div>
        <div class="row vertical-gap">
            <div class="col-lg-8">

                <!-- START: Latest Posts -->
                <h3 class="nk-decorated-h-2"><span><span class="text-main-1">Latest BFUN</span> News</span></h3>
                <div class="nk-gap"></div>
                <div class="nk-blog-grid">
                    <div class="row">


                        <div class="col-md-6">
                            <!-- START: Post -->
                            <div class="nk-blog-post">
                                <a href="#" class="nk-post-img">
                                    <img src="{{asset('images/main/chat_image.png')}}" alt="He made his passenger captain of one">
                                    <span class="nk-post-comments-count">13</span>
                                </a>
                                <div class="nk-gap"></div>
                                <h2 class="nk-post-title h4"><a href="#">Get new friend and Win the Prize </a></h2>
                                <div class="nk-post-by">
                                    <img src="assets/images/avatar-3.jpg" alt="Wolfenstein" class="rounded-circle" width="35"> by <a href="#">Albert</a>
                                </div>
                                <div class="nk-gap"></div>
                                <div class="nk-post-text">
                                    <p>Just then her head struck against the roof of the hall: in fact she was now more than nine feet high, and she at once took up the little golden key and hurried off to the garden door...</p>
                                </div>
                                <div class="nk-gap"></div>
                                <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">Read More</a>
                            </div>
                            <!-- END: Post -->
                        </div>


                        <div class="col-md-6">
                            <!-- START: Post -->
                            <div class="nk-blog-post">
                                <a href="#" class="nk-post-img">
                                    <img src="{{asset('images/main/mafia_games.png')}}" alt="At first, for some time, I was not able to answer">
                                    <span class="nk-post-comments-count">0</span>
                                </a>
                                <div class="nk-gap"></div>
                                <h2 class="nk-post-title h4"><a href="#">At first, for some time, I was not able to answer</a></h2>
                                <div class="nk-post-by">
                                    <img src="assets/images/avatar-3.jpg" alt="Wolfenstein" class="rounded-circle" width="35"> by <a href="#">Robert</a>
                                </div>
                                <div class="nk-gap"></div>
                                <div class="nk-post-text">
                                    <p>This little wandering journey, without settled place of abode, had been so unpleasant to me, that my own house, as I called it to myself, was a perfect settlement to me compared to that...</p>
                                </div>
                                <div class="nk-gap"></div>
                                <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-dark-3 nk-btn-hover-color-main-1">Read More</a>
                            </div>
                            <!-- END: Post -->
                        </div>

                    </div>
                </div>
                <!-- END: Latest Posts -->

                <!-- START: Latest Matches -->
                <div class="nk-gap-2"></div>
                <h3 class="nk-decorated-h-2"><span><span class="text-main-1">Latest</span> Matches</span></h3>
                <div class="nk-gap"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="nk-match-score bg-dark-3">
                            Now Playing
                        </div>
                        <div class="nk-gap-2"></div>
                        <div class="nk-widget-match p-0">
                            <div class="nk-widget-match-teams">
                                <div class="nk-widget-match-team-logo">
                                    <img src="{{asset('images/main/mafia_icon.png')}}" alt="">
                                </div>
                                <div class="nk-widget-match-vs">VS</div>
                                <div class="nk-widget-match-team-logo">
                                    <img src="{{asset('images/main/mafia_icon2.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="nk-gap-2"></div>
                        <p>As she said this she looked down at her hands and was surprised to see.</p>
                        <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-main-1">Match Details</a>
                    </div>
                    <div class="col-md-8">
                        <div class="responsive-embed responsive-embed-16x9">
                            <img src="{{asset('images/main/video_matches.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="nk-gap"></div>
                <div class="nk-match">
                    <div class="nk-match-team-left">
                        <a href="#">
                        <span class="nk-match-team-logo">
                            <img src="{{asset('images/main/team1.png')}}" alt="">
                        </span>
                            <span class="nk-match-team-name">
                            ROBUST
                        </span>
                        </a>
                    </div>
                    <div class="nk-match-status">
                        <a href="#">
                            <span class="nk-match-status-vs">VS</span>
                           <!-- <span class="nk-match-status-date">Apr 28, 2018 8:00 pm</span>-->
                            <span class="nk-match-score bg-danger">
                            2 : 17
                        </span>
                        </a>
                    </div>
                    <div class="nk-match-team-right">
                        <a href="#">
                        <span class="nk-match-team-name">
                            Cloud 9
                        </span>
                            <span class="nk-match-team-logo">
                            <img src="{{asset('images/main/team2.png')}}" alt="">
                        </span>
                        </a>
                    </div>
                </div>

                <div class="nk-match">
                    <div class="nk-match-team-left">
                        <a href="#">
                        <span class="nk-match-team-logo">
                            <img src="{{asset('images/main/team3.png')}}" alt="">
                        </span>
                            <span class="nk-match-team-name">
                            WEREWOLF
                        </span>
                        </a>
                    </div>
                    <div class="nk-match-status">
                        <a href="#">
                            <span class="nk-match-status-vs">VS</span>
                            <!-- <span class="nk-match-status-date">Apr 28, 2018 8:00 pm</span>-->
                            <span class="nk-match-score bg-success">
                            28 : 19
                        </span>
                        </a>
                    </div>
                    <div class="nk-match-team-right">
                        <a href="#">
                        <span class="nk-match-team-name">
                            BANDIT
                        </span>
                            <span class="nk-match-team-logo">
                            <img src="{{asset('images/main/team4.png')}}" alt="">
                        </span>
                        </a>
                    </div>
                </div>

                <div class="nk-match">
                    <div class="nk-match-team-left">
                        <a href="#">
                        <span class="nk-match-team-logo">
                            <img src="{{asset('images/main/team5.png')}}" alt="">
                        </span>
                            <span class="nk-match-team-name">
                            SOLOMON
                        </span>
                        </a>
                    </div>
                    <div class="nk-match-status">
                        <a href="#">
                            <span class="nk-match-status-vs">VS</span>
                            <!-- <span class="nk-match-status-date">Apr 28, 2018 8:00 pm</span>-->
                            <span class="nk-match-score bg-dark-1">
                            13 : 13
                        </span>
                        </a>
                    </div>
                    <div class="nk-match-team-right">
                        <a href="#">
                        <span class="nk-match-team-name">
                            TITANIC
                        </span>
                            <span class="nk-match-team-logo">
                            <img src="{{asset('images/main/team6.png')}}" alt="">
                        </span>
                        </a>
                    </div>
                </div>
                <!-- END: Latest Matches -->


                <!-- END: Best Selling -->
            </div>
            <div class="col-lg-4">
                <!--
                    START: Sidebar

                    Additional Classes:
                        .nk-sidebar-left
                        .nk-sidebar-right
                        .nk-sidebar-sticky
                -->
                <aside class="nk-sidebar nk-sidebar-right nk-sidebar-sticky">
                    <div class="nk-widget">
                        <div class="nk-widget-content">
                            <form action="#" class="nk-form nk-form-style-1" novalidate="novalidate">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Type something...">
                                    <button class="nk-btn nk-btn-color-main-1"><span class="ion-search"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="nk-widget nk-widget-highlighted">
                        <h4 class="nk-widget-title"><span><span class="text-main-1">Latest</span> Video</span></h4>
                        <div class="nk-widget-content">
                            <img src="{{asset('images/main/video_matches2.png')}}" alt="">
                        </div>
                    </div>
                    <div class="nk-widget nk-widget-highlighted">
                        <h4 class="nk-widget-title"><span><span class="text-main-1">Top 3</span> Recent</span></h4>
                        <div class="nk-widget-content">

                            <div class="nk-widget-post">
                                <a href="#" class="nk-post-image">
                                    <img src="{{asset('images/main/games_1.png')}}" alt="">
                                </a>
                                <h3 class="nk-post-title"><a href="#">Smell magic in the air. Or maybe barbecue</a></h3>
                                <div class="nk-post-date"><span class="fa fa-calendar"></span> Sep 18, 2018</div>
                            </div>

                            <div class="nk-widget-post">
                                <a href="#" class="nk-post-image">
                                    <img src="{{asset('images/main/games_2.png')}}" alt="">
                                </a>
                                <h3 class="nk-post-title"><a href="#">Grab your sword and fight the Horde</a></h3>
                                <div class="nk-post-date"><span class="fa fa-calendar"></span> Sep 5, 2018</div>
                            </div>

                            <div class="nk-widget-post">
                                <a href="#" class="nk-post-image">
                                    <img src="{{asset('images/main/games_3.png')}}" alt="">
                                </a>
                                <h3 class="nk-post-title"><a href="#">We found a witch! May we burn her?</a></h3>
                                <div class="nk-post-date"><span class="fa fa-calendar"></span> Aug 27, 2018</div>
                            </div>

                        </div>
                    </div>



                </aside>
                <!-- END: Sidebar -->
            </div>
        </div>
    </div>

    <div class="nk-gap-4"></div>



    <!-- START: Footer -->
    <footer class="nk-footer">



        <div class="nk-copyright">
            <div class="container">
                <div class="nk-copyright-left">
                    Copyright &copy; 2018 <a href="#" target="_blank">BFun</a>
                </div>
                <div class="nk-copyright-right">
                    <!--<ul class="nk-social-links-2">
                        <li><a class="nk-social-rss" href="#"><span class="fa fa-rss"></span></a></li>
                        <li><a class="nk-social-twitch" href="#"><span class="fab fa-twitch"></span></a></li>
                        <li><a class="nk-social-steam" href="#"><span class="fab fa-steam"></span></a></li>
                        <li><a class="nk-social-facebook" href="#"><span class="fab fa-facebook"></span></a></li>
                        <li><a class="nk-social-google-plus" href="#"><span class="fab fa-google-plus"></span></a></li>
                        <li><a class="nk-social-twitter" href="https://twitter.com/nkdevv" target="_blank"><span class="fab fa-twitter"></span></a></li>
                        <li><a class="nk-social-pinterest" href="#"><span class="fab fa-pinterest-p"></span></a></li> -->

                        <!-- Additional Social Buttons
                            <li><a class="nk-social-behance" href="#"><span class="fab fa-behance"></span></a></li>
                            <li><a class="nk-social-bitbucket" href="#"><span class="fab fa-bitbucket"></span></a></li>
                            <li><a class="nk-social-dropbox" href="#"><span class="fab fa-dropbox"></span></a></li>
                            <li><a class="nk-social-dribbble" href="#"><span class="fab fa-dribbble"></span></a></li>
                            <li><a class="nk-social-deviantart" href="#"><span class="fab fa-deviantart"></span></a></li>
                            <li><a class="nk-social-flickr" href="#"><span class="fab fa-flickr"></span></a></li>
                            <li><a class="nk-social-foursquare" href="#"><span class="fab fa-foursquare"></span></a></li>
                            <li><a class="nk-social-github" href="#"><span class="fab fa-github"></span></a></li>
                            <li><a class="nk-social-instagram" href="#"><span class="fab fa-instagram"></span></a></li>
                            <li><a class="nk-social-linkedin" href="#"><span class="fab fa-linkedin"></span></a></li>
                            <li><a class="nk-social-medium" href="#"><span class="fab fa-medium"></span></a></li>
                            <li><a class="nk-social-odnoklassniki" href="#"><span class="fab fa-odnoklassniki"></span></a></li>
                            <li><a class="nk-social-paypal" href="#"><span class="fab fa-paypal"></span></a></li>
                            <li><a class="nk-social-reddit" href="#"><span class="fab fa-reddit"></span></a></li>
                            <li><a class="nk-social-skype" href="#"><span class="fab fa-skype"></span></a></li>
                            <li><a class="nk-social-soundcloud" href="#"><span class="fab fa-soundcloud"></span></a></li>
                            <li><a class="nk-social-slack" href="#"><span class="fab fa-slack"></span></a></li>
                            <li><a class="nk-social-tumblr" href="#"><span class="fab fa-tumblr"></span></a></li>
                            <li><a class="nk-social-vimeo" href="#"><span class="fab fa-vimeo"></span></a></li>
                            <li><a class="nk-social-vk" href="#"><span class="fab fa-vk"></span></a></li>
                            <li><a class="nk-social-wordpress" href="#"><span class="fab fa-wordpress"></span></a></li>
                            <li><a class="nk-social-youtube" href="#"><span class="fab fa-youtube"></span></a></li>
                        -->
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- END: Footer -->


</div>




<!-- START: Page Background -->

<img class="nk-page-background-top" src="assets/images/bg-top.png" alt="">
<img class="nk-page-background-bottom" src="assets/images/bg-bottom.png" alt="">

<!-- END: Page Background -->




<!-- START: Search Modal -->
<div class="nk-modal modal fade" id="modalSearch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="ion-android-close"></span>
                </button>

                <h4 class="mb-0">Search</h4>

                <div class="nk-gap-1"></div>
                <form action="#" class="nk-form nk-form-style-1">
                    <input type="text" value="" name="search" class="form-control" placeholder="Type something and press Enter" autofocus>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END: Search Modal -->



<!-- START: Login Modal -->
<div class="nk-modal modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="ion-android-close"></span>
                </button>

                <h4 class="mb-0"><span class="text-main-1">Sign</span> In</h4>

                <div class="nk-gap-1"></div>
                <form action="#" class="nk-form text-white">
                    <div class="row vertical-gap">
                        <div class="col-md-6">
                            Use email and password:

                            <div class="nk-gap"></div>
                            <input type="email" value="" name="email" class=" form-control" placeholder="Email">

                            <div class="nk-gap"></div>
                            <input type="password" value="" name="password" class="required form-control" placeholder="Password">
                        </div>
                        <div class="col-md-6">
                            Or social account:

                            <div class="nk-gap"></div>

                            <ul class="nk-social-links-2">
                                <li><a class="nk-social-facebook" href="#"><span class="fab fa-facebook"></span></a></li>
                                <li><a class="nk-social-google-plus" href="#"><span class="fab fa-google-plus"></span></a></li>
                                <li><a class="nk-social-twitter" href="#"><span class="fab fa-twitter"></span></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="nk-gap-1"></div>
                    <div class="row vertical-gap">
                        <div class="col-md-6">
                            <a href="#" class="nk-btn nk-btn-rounded nk-btn-color-white nk-btn-block">Sign In</a>
                        </div>
                        <div class="col-md-6">
                            <div class="mnt-5">
                                <small><a href="#">Forgot your password?</a></small>
                            </div>
                            <div class="mnt-5">
                                <small><a href="#">Not a member? Sign up</a></small>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END: Login Modal -->




<!-- START: Scripts -->

<!-- Object Fit Polyfill -->
<script src="assets/vendor/object-fit-images/dist/ofi.min.js"></script>

<!-- GSAP -->
<script src="assets/vendor/gsap/src/minified/TweenMax.min.js"></script>
<script src="assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<!-- Popper -->
<script src="assets/vendor/popper.js/dist/umd/popper.min.js"></script>

<!-- Bootstrap -->
<script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Sticky Kit -->
<script src="assets/vendor/sticky-kit/dist/sticky-kit.min.js"></script>

<!-- Jarallax -->
<script src="assets/vendor/jarallax/dist/jarallax.min.js"></script>
<script src="assets/vendor/jarallax/dist/jarallax-video.min.js"></script>

<!-- imagesLoaded -->
<script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>

<!-- Flickity -->
<script src="assets/vendor/flickity/dist/flickity.pkgd.min.js"></script>

<!-- Photoswipe -->
<script src="assets/vendor/photoswipe/dist/photoswipe.min.js"></script>
<script src="assets/vendor/photoswipe/dist/photoswipe-ui-default.min.js"></script>

<!-- Jquery Validation -->
<script src="assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- Jquery Countdown + Moment -->
<script src="assets/vendor/jquery-countdown/dist/jquery.countdown.min.js"></script>
<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>

<!-- Hammer.js -->
<script src="assets/vendor/hammerjs/hammer.min.js"></script>

<!-- NanoSroller -->
<script src="assets/vendor/nanoscroller/bin/javascripts/jquery.nanoscroller.js"></script>

<!-- SoundManager2 -->
<script src="assets/vendor/soundmanager2/script/soundmanager2-nodebug-jsmin.js"></script>

<!-- Seiyria Bootstrap Slider -->
<script src="assets/vendor/bootstrap-slider/dist/bootstrap-slider.min.js"></script>

<!-- Summernote -->
<script src="assets/vendor/summernote/dist/summernote-bs4.min.js"></script>

<!-- nK Share -->
<script src="assets/plugins/nk-share/nk-share.js"></script>

<!-- GoodGames -->
<script src="assets/js/goodgames.min.js"></script>
<script src="assets/js/goodgames-init.js"></script>
<!-- END: Scripts -->



</body>
</html>
