<!-- Start OUr Team Section -->
<div class="section section-pad section-bg team-section section-gradiant section-fix bg-fixed" id="team">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-head-s2">
                    <h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Our Team</h6>
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">The Leadership Team</h2>
                    <p class="animated" data-animate="fadeInUp" data-delay=".2">BLUECHIPS Team combines a passion for esports, industry experise &amp; proven record in finance, development, marketing &amp; licensing.</p>
                </div>
            </div><!-- .col  -->
            <div class="iframe-center">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Yd5k-Y3HS6E" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>


        </div><!-- .row  -->
        <div class="row">
            <div class="col-md-4 col-lg-4 offset-lg-2 offset-xl-0 col-xl-4 d-inline-flex justify-content-center">
                <div class="team-member animated" data-animate="fadeInUp" data-delay=".2">
                    <div class="team-photo">
                        <img src="{{asset('images/main/george.jpg')}}" alt="team">
                        <a href="#" class="expand-trigger content-popup"></a>
                    </div>
                    <div class="team-info">
                        <h5 class="team-name">George <br>Espliguera</h5>
                        <span class="team-title">CEO &amp; Founder </span>
                        <ul class="team-social">
                            <li><a href="https://www.facebook.com/profile.php?id=100000893451940"><em class="fab fa-facebook-f"></em></a></li>
                            <li><a href="https://www.linkedin.com/in/george-bchip/"><em class="fab fa-linkedin-in"></em></a></li>
                            <!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->
                        </ul>
                    </div>

                    <!-- Start .team-profile  -->
                    <div id="team-profile-1" class="team-profile mfp-hide">
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        <div class="container-fluid">
                            <div class="row no-mg">
                                <div class="col-md-6">
                                    <div class="team-profile-photo">
                                        <img src="{{asset('images/main/andrew _liu.jpg')}}" alt="team" />
                                    </div>
                                </div><!-- .col  -->

                                <div class="col-md-6">
                                    <div class="team-profile-info">
                                        <h3 class="name">Andrew Liu</h3>
                                        <p class="sub-title">CEO &amp; Lead Blockchain </p>
                                        <ul class="tpi-social">
                                            <li><a href=""><em class="fab fa-facebook-f"></em></a></li>
                                            <li><a href=""><em class="fab fa-linkedin-in"></em></a></li>
                                            <li><a href=""><em class="fab fa-twitter"></em></a></li>
                                        </ul>
                                        <p>He started his career in technology and information
                                            in 2008, then tried to do business in 2011 and
                                            established a small company at that time in Hong
                                            Kong. Then expand innovation and ideas through
                                            cryptocurrency. </p>

                                    </div>
                                </div><!-- .col  -->

                            </div><!-- .row  -->
                        </div><!-- .container  -->
                    </div><!-- .team-profile  -->

                </div>
            </div><!-- .col  -->

            <div class="col-md-4 col-lg-4 offset-lg-2 offset-xl-0 col-xl-4 d-inline-flex justify-content-center">
                <div class="team-member animated" data-animate="fadeInUp" data-delay=".3">
                    <div class="team-photo">
                        <img src="{{asset('images/main/john.jpg')}}" alt="team">
                        <a href="#" class="expand-trigger content-popup"></a>
                    </div>
                    <div class="team-info">
                        <h5 class="team-name">John <br>Leo</h5>
                        <span class="team-title">COO &amp; Co Founder</span>
                        <ul class="team-social">
                            <li><a href="https://www.facebook.com/profile.php?id=754502937"><em class="fab fa-facebook-f"></em></a></li>
                            <li><a href="http://www.linkedin.com/in/draper-leo-a5698ab1"><em class="fab fa-linkedin-in"></em></a></li>
                            <!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->
                        </ul>
                    </div>

                    <!-- Start .team-profile  -->
                    <div id="team-profile-2" class="team-profile mfp-hide">
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        <div class="container-fluid">
                            <div class="row no-mg">
                                <div class="col-md-6">
                                    <div class="team-profile-photo">
                                        <img src="{{asset('images/main/raymond_wang.jpg')}}" alt="team" />
                                    </div>
                                </div><!-- .col  -->

                                <div class="col-md-6">
                                    <div class="team-profile-info">
                                        <h3 class="name">Raymond Wang</h3>
                                        <p class="sub-title">Blockchain Expert</p>
                                        <ul class="tpi-social">
                                            <li><a href=""><em class="fab fa-facebook-f"></em></a></li>
                                            <li><a href=""><em class="fab fa-linkedin-in"></em></a></li>
                                            <li><a href=""><em class="fab fa-twitter"></em></a></li>
                                        </ul>
                                        <p>Raymond Wang has served as a representative of
                                            the blockchain community in Hong Kong.
                                            Currently, he has experience in blockchain as an
                                            initial investor and businessman involved in ICO
                                            project analysis, marketing, incubation and
                                            consultation for investors.</p>


                                    </div>
                                </div><!-- .col  -->

                            </div><!-- .row  -->
                        </div><!-- .container  -->
                    </div><!-- .team-profile  -->

                </div>
            </div><!-- .col  -->

            <div class="col-md-4 col-lg-4 offset-lg-2 offset-xl-0 col-xl-4 d-inline-flex justify-content-center">
                <div class="team-member animated" data-animate="fadeInUp" data-delay=".4">
                    <div class="team-photo">
                        <img src="{{asset('images/main/patrick.jpg')}}" alt="team">
                        <a href="#" class="expand-trigger content-popup"></a>
                    </div>
                    <div class="team-info">
                        <h5 class="team-name">Patrick <br>Antony</h5>
                        <span class="team-title">Blockchain <br>Developer</span>
                        <ul class="team-social">
                            <li><a href="https://www.facebook.com/cyptopatty"><em class="fab fa-facebook-f"></em></a></li>
                            <li><a href="http://www.linkedin.com/in/patrick-anthony-potestas-3b3767171"><em class="fab fa-linkedin-in"></em></a></li>
                            <!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->
                        </ul>
                    </div>

                    <!-- Start .team-profile  -->
                    <div id="team-profile-3" class="team-profile mfp-hide">
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        <div class="container-fluid">
                            <div class="row no-mg">
                                <div class="col-md-6">
                                    <div class="team-profile-photo">
                                        <img src="{{asset('images/main/shen_ru.jpg')}}" alt="team" />
                                    </div>
                                </div><!-- .col  -->

                                <div class="col-md-6">
                                    <div class="team-profile-info">
                                        <h3 class="name">Shen Ru</h3>
                                        <p class="sub-title">Advisor</p>
                                        <ul class="tpi-social">
                                            <li><a href=""><em class="fab fa-facebook-f"></em></a></li>
                                            <li><a href=""><em class="fab fa-linkedin-in"></em></a></li>
                                            <li><a href=""><em class="fab fa-twitter"></em></a></li>
                                        </ul>
                                        <p>Graduate of economics with an MBA in financial
                                            management from The University of Sydney. Based
                                            in China and actively involved in developing the
                                            ICO market and offering crypto funds. Has over 7
                                            years of experience in sales and business
                                            development management. </p>


                                    </div>
                                </div><!-- .col  -->

                            </div><!-- .row  -->
                        </div><!-- .container  -->
                    </div><!-- .team-profile  -->

                </div>
            </div><!-- .col  -->


            <div class="col-md-4 col-lg-4 offset-lg-2 offset-xl-0 col-xl-4 d-inline-flex justify-content-center">
                <div class="team-member animated" data-animate="fadeInUp" data-delay=".4">
                    <div class="team-photo">
                        <img src="{{asset('images/main/uwascott_advisor.jpg')}}" alt="team">
                        <a href="#" class="expand-trigger content-popup"></a>
                    </div>
                    <div class="team-info">
                        <h5 class="team-name">Uwa <br> Scott</h5>
                        <span class="team-title">Advisor</span>
                        <ul class="team-social">
                            <!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->
                            <li><a href="http://linkedin.com/in/uwa-scott-331759111"><em class="fab fa-linkedin-in"></em></a></li>
                            <!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->
                        </ul>
                    </div>

                    <!-- Start .team-profile  -->
                    <div id="team-profile-3" class="team-profile mfp-hide">
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        <div class="container-fluid">
                            <div class="row no-mg">
                                <div class="col-md-6">
                                    <div class="team-profile-photo">
                                        <img src="{{asset('images/main/shen_ru.jpg')}}" alt="team" />
                                    </div>
                                </div><!-- .col  -->

                                <div class="col-md-6">
                                    <div class="team-profile-info">
                                        <h3 class="name">Shen Ru</h3>
                                        <p class="sub-title">Advisor</p>
                                        <ul class="tpi-social">
                                            <li><a href=""><em class="fab fa-facebook-f"></em></a></li>
                                            <li><a href=""><em class="fab fa-linkedin-in"></em></a></li>
                                            <li><a href=""><em class="fab fa-twitter"></em></a></li>
                                        </ul>
                                        <p>Graduate of economics with an MBA in financial
                                            management from The University of Sydney. Based
                                            in China and actively involved in developing the
                                            ICO market and offering crypto funds. Has over 7
                                            years of experience in sales and business
                                            development management. </p>


                                    </div>
                                </div><!-- .col  -->

                            </div><!-- .row  -->
                        </div><!-- .container  -->
                    </div><!-- .team-profile  -->

                </div>
            </div>


            <div class="col-md-4 col-lg-4 offset-lg-2 offset-xl-0 col-xl-4 d-inline-flex justify-content-center">
                <div class="team-member animated" data-animate="fadeInUp" data-delay=".4">
                    <div class="team-photo">
                        <img src="{{asset('images/main/jerry_advisor.jpg')}}" alt="team">
                        <a href="#" class="expand-trigger content-popup"></a>
                    </div>
                    <div class="team-info">
                        <h5 class="team-name">Jerry <br> Gunandar</h5>
                        <span class="team-title">Advisor</span>
                        <ul class="team-social">
                            <li><a href="https://www.facebook.com/profile.php?id=100009483350536"><em class="fab fa-facebook-f"></em></a></li>
                            <li><a href="https://www.linkedin.com/in/jerry-gunandar-ba17b274"><em class="fab fa-linkedin-in"></em></a></li>
                            <!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->
                        </ul>
                    </div>

                    <!-- Start .team-profile  -->
                    <div id="team-profile-3" class="team-profile mfp-hide">
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        <div class="container-fluid">
                            <div class="row no-mg">
                                <div class="col-md-6">
                                    <div class="team-profile-photo">
                                        <img src="{{asset('images/main/shen_ru.jpg')}}" alt="team" />
                                    </div>
                                </div><!-- .col  -->

                                <div class="col-md-6">
                                    <div class="team-profile-info">
                                        <h3 class="name">Shen Ru</h3>
                                        <p class="sub-title">Advisor</p>
                                        <ul class="tpi-social">
                                            <li><a href=""><em class="fab fa-facebook-f"></em></a></li>
                                            <li><a href=""><em class="fab fa-linkedin-in"></em></a></li>
                                            <li><a href=""><em class="fab fa-twitter"></em></a></li>
                                        </ul>
                                        <p>Graduate of economics with an MBA in financial
                                            management from The University of Sydney. Based
                                            in China and actively involved in developing the
                                            ICO market and offering crypto funds. Has over 7
                                            years of experience in sales and business
                                            development management. </p>


                                    </div>
                                </div><!-- .col  -->

                            </div><!-- .row  -->
                        </div><!-- .container  -->
                    </div><!-- .team-profile  -->

                </div>
            </div>



            <div class="col-md-4 col-lg-6 offset-lg-2 offset-xl-0 col-xl-4 d-inline-flex justify-content-center">
                <div class="team-member animated" data-animate="fadeInUp" data-delay=".4">
                    <div class="team-photo">
                        <img src="{{asset('images/main/nasution_jatur_atmaja_advisor.jpg')}}" alt="team">
                        <a href="#" class="expand-trigger content-popup"></a>
                    </div>
                    <div class="team-info">
                        <h5 class="team-name">Nasution Jatur <br> Atmaja</h5>
                        <span class="team-title">Advisor</span>
                        <ul class="team-social">
                            <li><a href="https://www.facebook.com/profile.php?id=100000588496360"><em class="fab fa-facebook-f"></em></a></li>
                            <li><a href="https://www.linkedin.com/in/nasution-jatur-atmaja-8b2a19131"><em class="fab fa-linkedin-in"></em></a></li>
                            <!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->
                        </ul>
                    </div>

                    <!-- Start .team-profile  -->
                    <div id="team-profile-3" class="team-profile mfp-hide">
                        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        <div class="container-fluid">
                            <div class="row no-mg">
                                <div class="col-md-6">
                                    <div class="team-profile-photo">
                                        <img src="{{asset('images/main/shen_ru.jpg')}}" alt="team" />
                                    </div>
                                </div><!-- .col  -->

                                <div class="col-md-6">
                                    <div class="team-profile-info">
                                        <h3 class="name">Shen Ru</h3>
                                        <p class="sub-title">Advisor</p>
                                        <ul class="tpi-social">
                                            <li><a href=""><em class="fab fa-facebook-f"></em></a></li>
                                            <li><a href=""><em class="fab fa-linkedin-in"></em></a></li>
                                            <li><a href=""><em class="fab fa-twitter"></em></a></li>
                                        </ul>
                                        <p>Graduate of economics with an MBA in financial
                                            management from The University of Sydney. Based
                                            in China and actively involved in developing the
                                            ICO market and offering crypto funds. Has over 7
                                            years of experience in sales and business
                                            development management. </p>


                                    </div>
                                </div><!-- .col  -->

                            </div><!-- .row  -->
                        </div><!-- .container  -->
                    </div><!-- .team-profile  -->

                </div>
            </div>





        </div>
    </div> <!-- .col  -->

</div><!-- .row  -->
<!-- Start Section -->