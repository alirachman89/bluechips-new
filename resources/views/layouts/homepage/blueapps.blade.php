<!-- Start Bluechips Apps Section -->
<div class="section section-pad section-bg-alt section-pro nopb" id="apps">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-head-s2">
                    <h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">BLUECHIPS Apps</h6>
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">BLUECHIPS Mobile App</h2>
                    <p class="animated" data-animate="fadeInUp" data-delay=".2">Once you’ve entered into our ecosystem, you can manage every thing. Anyone with a smartphone and an internet connection can participate in global marketplace.</p>
                </div>
            </div><!-- .col  -->
        </div><!-- .row  -->

        <div class="row align-items-center">
            <div class="col-md-6 res-m-btm">
                <div class="text-block">
                    <!--<p class="animated" data-animate="fadeInUp" data-delay="0">Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc itation ullamco laboris nisi commodo consequat. </p>-->
                    <ul class="animated" data-animate="fadeInUp" data-delay=".1">
                        <li>Crypto-news curation</li>
                        <li>Natural Language Understanding</li>
                        <li>Wallet aggregation</li>
                        <li>Professional Network</li>
                        <li>No more expensive fees</li>
                    </ul>
                    <ul class="btns">
                        <li class="animated" data-animate="fadeInUp" data-delay="0"><a href="construction" class="btn btn-sm">GET THE APP NOW</a></li>
                        <li class="animated" data-animate="fadeInUp" data-delay=".2">
                            <a href="#"><em class="fab fa-apple"></em></a>
                            <a href="#"><em class="fab fa-android"></em></a>
                            <a href="#"><em class="fab fa-windows"></em></a>
                        </li>
                    </ul>
                </div>
            </div><!-- .col  -->
            <div class="col-md-6 mt-auto ">
                <div class="animated" data-animate="fadeInUp" data-delay="0">
                    <img src="{{asset('images/main/mobile-apps.png')}}" alt="graph">
                </div>
            </div><!-- .col  -->
        </div><!-- .row  -->
    </div><!-- .container  -->
</div>
<!-- Start Section -->