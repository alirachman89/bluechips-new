<!-- Start Roadmap Section RoadMap-->
<div class="section section-pad section-bg-alt section-pro-alt roadmap-section" id="roadmap">
    <!--<div class="section section-pad section-bg section-connect section-light" id="roadmap">-->
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 offset-lg-3">
                <!--<div class="col">-->
                <div class="section-head-s2">
                    <!--<div class="section-head">-->
                    <h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Roadmap</h6>
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">The Timeline</h2>
                    <p class="animated" data-animate="fadeInUp" data-delay=".2">With help from our teams, contributors and investors these are the milestones we are looking forward to achieve.</p>
                </div>
            </div><!-- .col  -->
        </div><!-- .row  -->



        <div class="row roadmap-list align-items-end animated" data-animate="fadeInUp" data-delay="0">
            <div class="col-lg">
                <div class="single-roadmap roadmap-sm roadmap-done">
                    <h6>Q2 2018</h6>
                    <p>Concept idea of the BLUECHIPS Creation of the BLUECHIPS.</p>
                </div>
            </div><!-- .col  -->
            <div class="col-lg width-0">
                <div class="single-roadmap roadmap-down roadmap-done">
                    <h6>Q3 2018</h6>
                    <p>Build infrastructure and website development</p>
                    <p>Expand teams and networks for marketing</p>
                    <p>Development of smart contract for the storage of basic information</p>
                </div>
            </div><!-- .col  -->
            <div class="col-lg">
                <div class="single-roadmap roadmap-lg">
                    <h6>Q4 2018</h6>
                    <!--<p><span>October</span></p>-->
                    <ul>
                        <li>Token Sale</li>
                        <li>Listing BLUECHIPS on Exchanges</li>
                        <li>Distribution Airdrop</li>
                    </ul>
                </div>
            </div><!-- .col  -->
            <div class="col-lg ">
                <div class="single-roadmap roadmap-down">
                    <h6>Q1 2019</h6>
                    <ul>
                        <li>Opening the website version Bfun</li>
                        <li>Release Bfun Mobile Application</li>
                    </ul>
                </div>
            </div><!-- .col  -->
            <div class="col-lg">
                <div class="single-roadmap roadmap-sm">
                    <h6>Q2 2019</h6>
                    <ul>
                        <li>Token Burn 40% from the Circulating Supply</li>
                        <li>Soft Launch BCHIP DAPP</li>
                    </ul>
                </div>
            </div><!-- .col  -->
            <div class="col-lg ">
                <div class="single-roadmap roadmap-down">
                    <h6>Q3 2019</h6>
                    <ul>
                        <li>Launch of the beta version BTUBE</li>
                    </ul>
                </div>
            </div><!-- .col  -->
            <div class="col-lg">
                <div class="single-roadmap roadmap-sm">
                    <h6>Q4 2019</h6>
                    <ul>
                        <li>Final Release BTUBE</li>
                    </ul>
                </div>
            </div><!-- .col  -->
        </div><!-- .row  -->
    </div><!-- .container  -->
</div>
<!-- Start Section -->