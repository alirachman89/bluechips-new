<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="BlueChips Token | The Masterpiece of Decentralized in Entertainment">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}">
    <!-- Site Title  -->
    <title>BlueChips - Token</title>
    <!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor.bundle.css?ver=142')}}">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css?ver=142')}}">
    <link rel="stylesheet" href="{{asset('assets/css/theme.css?ver=142')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>



</head>
<body class="theme-dark io-azure io-azure-pro" data-spy="scroll" data-target="#mainnav" data-offset="80">

<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 gambar">
            <img src="{{asset('images/main/maintenance_image.png')}}">
        </div>
        <div class="col-md-12 col-sm-12 sorry-text">
            <h3>Sorry, We're doing some work on site!</h3>
        </div>
        <div class="col-md-12 sol-sm-12 thanks-text">
            <p>Thank you for being patient. We are doing some work on the site and will be back shortly.</p>
        </div>
    </div>
</div>

</body>
</html>
