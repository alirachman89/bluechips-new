<!-- Start Contact Section -->
<div class="section section-pad section-bg section-pro contact-section" id="contact">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-head-s2">
                    <h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Contact</h6>
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Get In Touch</h2>
                </div>
            </div><!-- .col  -->
        </div><!-- .row  -->
        <div class="row">
            <div class="col-lg-4 offset-lg-1">
                <div class="gaps"></div>
                <p class="animated" data-animate="fadeInUp" data-delay="0">Any question? Reach out to us and we’ll get back to you shortly.</p>
                <ul class="contact-info-alt">

                    <li class="animated" data-animate="fadeInUp" data-delay=".15"><em class="fa fa-envelope"></em><span>support@bluechips-token.com</span></li>
                    <li class="animated" data-animate="fadeInUp" data-delay=".2"><em class="fa fa-paper-plane"></em><span>Join us on Telegram</span></li>
                </ul>
            </div><!-- .col  -->
            <div class="col-lg-5">
                <form id="contact-form" class="form-message text-left" action="{{asset('form/contact.php')}}" method="post">
                    <div class="form-results"></div>
                    <div class="input-field animated" data-animate="fadeInUp" data-delay=".3">
                        <input name="contact-name" type="text" class="input-line required">
                        <label class="input-title">Your Name</label>
                    </div>
                    <div class="input-field animated" data-animate="fadeInUp" data-delay=".4">
                        <input name="contact-email" type="email" class="input-line required email">
                        <label class="input-title">Your Email</label>
                    </div>
                    <div class="input-field animated" data-animate="fadeInUp" data-delay=".5">
                        <textarea name="contact-message" class="txtarea input-line required"></textarea>
                        <label class="input-title">Your Message</label>
                    </div>
                    <input type="text" class="d-none" name="form-anti-honeypot" value="">
                    <div class="input-field animated" data-animate="fadeInUp" data-delay=".6">
                        <button type="submit" class="btn">Submit</button>
                    </div>
                </form>
            </div><!-- .col  -->
        </div><!-- .row  -->
    </div><!-- .container  -->
    <div class="mask-ov-right mask-ov-s6"></div><!-- .mask overlay -->
</div>
<!-- End Section -->