<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    /**public function index()
    {
        return view('home');
    } **/

    public function homepage()
    {
        return view('layouts.homepage.homepage');
    }

    public function construction()
    {
        return view('layouts.homepage.construction');
    }

    public function bfun()
    {
        return view('layouts.homepage.bfun');
    }

    public function buytoken()
    {
        return view('layouts.homepage.buytokens');
    }

}
